async = require('co')
u = require './utils'

module.exports = m = {}

noop = ->

m.version = ->
  'v0.2.1'

# caching commonly used variables
$document = undefined
$requestAnimationFrame = undefined
$cancelAnimationFrame = undefined


# self invoking function needed because of the way mocks work

initialize = (window) ->
  $document = window.document
  $cancelAnimationFrame = window.cancelAnimationFrame or window.clearTimeout
  $requestAnimationFrame = window.requestAnimationFrame or window.setTimeout
initialize window
# testing API

m.deps = (mock) ->
  initialize `window = mock || window`
  window


m.component = (component) ->
  args = []
  originalCtrl = component.controller or noop
  originalView = component.view or noop

  Ctrl = ->
    originalCtrl.apply(this, args)
    return this

  view = (ctrl) ->
    rest = [ ctrl ].concat(args)
    i = 1
    while i < arguments.length
      rest.push arguments[i]
      i++
    originalView.apply component, rest

  i = 1
  while i < arguments.length
    args.push arguments[i]
    i++
  if `originalCtrl != noop`
    Ctrl.prototype = originalCtrl.prototype
  view.$original = originalView
  output =
    controller: Ctrl
    view: view
  if args[0] and `args[0].key != null`
    output.attrs = key: args[0].key
  output

# This function was causing deopts in Chrome.

dataToString = (data) ->
  # data.toString() might throw or return null if data is the return
  # value of Console.log in some versions of Firefox
  try
    if `data != null` and `data.toString() != null`
      return data
  catch e
    # Swallow all errors here.
  ''

flatten = (list) ->
  # recursively flatten array
  i = 0
  while i < list.length
    if u.isArray(list[i])
      list = list.concat.apply([], list)
      # check current index again while there is an array at this
      # index.
      i--
    i++
  list

insertNode = (parent, node, index) ->
  parent.insertBefore node, parent.childNodes[index] or null

# the below recursively manages creation/diffing/removal of DOM elements
# based on comparison between `data` and `cached`
#
# the diff algorithm can be summarized as this:
# 1) compare `data` and `cached`
# 2) if they are different, copy `data` to `cached` and update the DOM
#    based on what the difference is
# 3) recursively apply this algorithm for every array and for the
#    children of every virtual element
#
# the `cached` data structure is essentially the same as the previous
# redraw's `data` data structure, with a few additions:
# - `cached` always has a property called `nodes`, which is a list of
#    DOM elements that correspond to the data represented by the
#    respective virtual element
# - in order to support attaching `nodes` as a property of `cached`,
#    `cached` is *always* a non-primitive object, i.e. if the data was
#    a string, then cached is a String instance. If data was `null` or
#    `undefined`, cached is `new String("")`
# - `cached also has a `cfgCtx` property, which is the state
#    storage object exposed by config(element, isInitialized, context)
# - when `cached` is an Object, it represents a virtual element; when
#    it's an Array, it represents a list of elements; when it's a
#    String, Number or Boolean, it represents a text node
#
# `parent` is a DOM element used for W3C DOM API calls
# `pTag` is only used for handling a corner case for textarea values
# `pCache` is used to remove nodes in some multi-node cases
# `pIndex` and `index` are used to figure out the offset of nodes.
# They're artifacts from before arrays started being flattened and are
# likely refactorable
# `data` and `cached` are, respectively, the new and old nodes being
# diffed
# `reattach` is a flag indicating whether a parent node was recreated (if
# so, and if this node is reused, then this node must reattach itself to
# the new parent)
# `editable` is a flag that indicates whether an ancestor is
# contenteditable
# `ns` indicates the closest HTML namespace as it cascades down from an
# ancestor
# `cfgs` is a list of config functions to run after the topmost build call
# finishes running
#
# there's logic that relies on the assumption that null and undefined
# data are equivalent to empty strings
# - this prevents lifecycle surprises from procedural helpers that mix
#   implicit and explicit return statements (e.g.
#   function foo() {if (cond) return m("div")}
# - it simplifies diffing code

buildContext = (parentElement, parentTag, parentCache, parentIndex, data, cached, shouldReattach, index, editable, namespace, configs) ->
  return {
    parent: parentElement
    pTag: parentTag
    pCache: parentCache
    pIndex: parentIndex
    data: data
    cached: cached
    reattach: shouldReattach
    index: index
    editable: editable
    ns: namespace
    cfgs: configs
  }

builderBuild = (inst) ->
  inst.data = dataToString(inst.data)
  if inst.data.subtree is 'retain'
    return inst.cached
  builderMakeCache inst
  if u.isArray(inst.data)
    builderBuildArray inst
  else if inst.data? and u.isObject(inst.data)
    builderBuildObject inst
  else if u.isFunction(inst.data)
    inst.cached
  else
    builderHandleTextNode inst



builderMakeCache = (inst) ->
  if inst.cached?
    if u.type.call(inst.cached) is u.type.call(inst.data)
      return
    if inst.pCache and inst.pCache.nodes
      offset = inst.index - (inst.pIndex)
      end = offset + (if u.isArray(inst.data) then inst.data else inst.cached.nodes).length
      clear inst.pCache.nodes.slice(offset, end), inst.pCache.slice(offset, end)
    else if inst.cached.nodes
      clear inst.cached.nodes, inst.cached
  inst.cached = new (inst.data.constructor)
  # if constructor creates a virtual dom element, use a blank object as
  # the base cached node instead of copying the virtual el (#277)
  if inst.cached.tag
    inst.cached = {}
  inst.cached.nodes = []

buildArrayKeys = (data) ->
  guid = 0
  u.forKeys data, ->
    u.forEach data, (attrs) ->
      attrs = attrs and attrs.attrs
      if attrs and not attrs.key?
        attrs.key = '__mithril__' + guid++
    true

builderBuildArrayChild = (inst, child, cached, count) ->
  builderBuild buildContext(
    inst.parent,
    inst.pTag,
    inst.cached,
    inst.index,
    child,
    cached,
    inst.reattach,
    (inst.index + count) or count,
    inst.editable,
    inst.ns,
    inst.cfgs
  )



DELETION = 1
INSERTION = 2
MOVE = 3

# This is by far the most performance-sensitive method here. If you make
# any changes, be careful to avoid performance regressions. Note that
# variable caching doesn't help, even in the loop.

builderBuildArray = (inst) ->
  inst.data = flatten(inst.data)
  nodes = []
  intact = inst.cached.length is inst.data.length
  subArrayCount = 0
  # keys algorithm:
  # sort elements without recreating them if keys are present
  #
  # 1) create a map of all existing keys, and mark all for deletion
  # 2) add new keys to map and mark them for addition
  # 3) if key exists in new list, change action from deletion to a move
  # 4) for each key, handle its corresponding action as marked in
  #    previous steps
  existing = {}
  shouldMaintainIdentities = false
  u.forKeys inst.cached, (attrs, i) ->
    shouldMaintainIdentities = true
    existing[attrs.key] =
      action: DELETION
      index: i
  buildArrayKeys inst.data
  if shouldMaintainIdentities
    builderDiffKeys inst, existing
  # end key algorithm
  # don't change: faster than u.forEach
  cacheCount = 0
  i = 0
  len = inst.data.length
  while i < len
    # diff each item in the array
    item = builderBuildArrayChild(inst, inst.data[i], inst.cached[cacheCount], subArrayCount)
    if `item != undefined`
      intact = intact and item.nodes.intact
      subArrayCount += getSubArrayCount(item)
      inst.cached[cacheCount++] = item
    i++
  if !intact
    builderDiffArray inst, nodes
  inst.cached

builderDiffKeys = (inst, existing) ->
  keysDiffer = `inst.data.length != inst.cached.length`
  if !keysDiffer
    u.forKeys inst.data, (attrs, i) ->
      cachedCell = inst.cached[i]
      keysDiffer = cachedCell and cachedCell.attrs and `cachedCell.attrs.key != attrs.key`
  if keysDiffer
    builderHandleKeysDiffer inst, existing

builderHandleKeysDiffer = (inst, existing) ->
  cached = inst.cached.nodes
  u.forKeys inst.data, (key, i) ->
    key = key.key
    if existing[key]
      existing[key] =
        action: MOVE
        index: i
        from: existing[key].index
        element: cached[existing[key].index] or $document.createElement('div')
    else
      existing[key] =
        action: INSERTION
        index: i
  actions = []
  u.forOwn existing, (value) ->
    actions.push value
  changes = actions.sort(sortChanges)
  newCached = new Array(inst.cached.length)
  newCached.nodes = inst.cached.nodes.slice()
  u.forEach changes, (change) ->
    index = change.index
    switch change.action
      when DELETION
        clear inst.cached[index].nodes, inst.cached[index]
        newCached.splice index, 1
      when INSERTION
        dummy = $document.createElement('div')
        dummy.key = inst.data[index].attrs.key
        insertNode inst.parent, dummy, index
        newCached.splice index, 0,
          attrs: key: inst.data[index].attrs.key
          nodes: [ dummy ]
        newCached.nodes[index] = dummy
      when MOVE
        changeElement = change.element
        # changeElement is never null
        if `inst.parent.childNodes[index] != changeElement`
          inst.parent.insertBefore changeElement, inst.parent.childNodes[index] or null
        newCached[index] = inst.cached[change.from]
        newCached.nodes[index] = changeElement
  inst.cached = newCached

# diffs the array itself

builderDiffArray = (inst, nodes) ->
  # update the list of DOM nodes by collecting the nodes from each item
  i = 0
  len = inst.data.length
  while i < len
    item = inst.cached[i]
    if `item != null`
      nodes.push.apply nodes, item.nodes
    i++
  # remove items from the end of the array if the new array is shorter
  # than the old one. if errors ever happen here, the issue is most
  # likely a bug in the construction of the `cached` data structure
  # somewhere earlier in the program
  u.forEach inst.cached.nodes, (node, i) ->
    if `node.parentNode != null` and nodes.indexOf(node) < 0
      clear [ node ], [ inst.cached[i] ]
  if inst.data.length < inst.cached.length
    inst.cached.length = inst.data.length
  inst.cached.nodes = nodes

builderInitAttrs = (inst) ->
  dataAttrs = inst.data.attrs = inst.data.attrs or {}
  inst.cached.attrs = inst.cached.attrs or {}
  dataAttrKeys = Object.keys(inst.data.attrs)
  builderMaybeRecreateObject inst, dataAttrKeys
  dataAttrKeys.length > +('key' of dataAttrs)

builderGetObjectNamespace = (inst) ->
  data = inst.data
  if data.attrs.xmlns
    return data.attrs.xmlns
  else if data.tag is 'svg'
    return 'http://www.w3.org/2000/svg'
  else if data.tag is 'math'
    return 'http://www.w3.org/1998/Math/MathML'
  else return inst.ns

builderBuildObject = (inst) ->
  views = []
  controllers = []
  builderMarkViews inst, views, controllers
  if not inst.data.tag and controllers.length
    throw new Error('Component template must return a virtual ' + 'element, not an array, string, etc.')
  hasKeys = builderInitAttrs(inst)
  if u.isString(inst.data.tag)
    return objectBuild(
      builder: inst
      hasKeys: hasKeys
      views: views
      controllers: controllers
      ns: builderGetObjectNamespace(inst))

builderMarkViews = (inst, views, controllers) ->
  cached = inst.cached and inst.cached.controllers
  while inst.data.view?
    builderCheckView inst, cached, controllers, views

builderCheckView = (inst, cached, controllers, views) ->
  view = inst.data.view.$original or inst.data.view

  controller = getController(inst.cached.views, view, cached, inst.data.controller)
  # Faster to coerce to number and check for NaN
  key = +(inst.data and inst.data.attrs and inst.data.attrs.key)
  inst.data = inst.data.view(controller)
  if inst.data.subtree is 'retain'
    return inst.cached
  if `key == key`
    (inst.data.attrs = inst.data.attrs or {}).key = key
  updateLists views, controllers, view, controller

unloaderHandler = (inst, ev) ->
  inst.ctrls.splice inst.ctrls.indexOf(inst.ctrl), 1
  inst.views.splice inst.views.indexOf(inst.view), 1
  if inst.ctrl and u.isFunction(inst.ctrl.onunload)
    inst.ctrl.onunload ev

updateLists = (views, controllers, view, controller) ->
  views.push view
  unloaders[controllers.push(controller) - 1] =
    views: views
    view: view
    ctrl: controller
    ctrls: controllers

redrawing = false

m.redraw = (force) ->
  return if redrawing
  redrawing = true
  try
    attemptRedraw force
  finally
    redrawing = false


redrawStrategy = ''
m.redraw.strategy = (set)->
  if set? then redrawStrategy = set else return redrawStrategy

getController = (views, view, cached, controller) ->
  index = if `redrawStrategy == 'diff'` and views then views.indexOf(view) else -1
  if index > -1
    return cached[index]
  else if u.isFunction(controller)
    return new controller
  else
    return {}

builderMaybeRecreateObject = (inst, dataAttrKeys) ->
  # if an element is different enough from the one in cache, recreate it
  if builderElemIsDifferentEnough(inst, dataAttrKeys)
    if inst.cached.nodes.length
      clear inst.cached.nodes
    if inst.cached.cfgCtx and u.isFunction(inst.cached.cfgCtx.onunload)
      inst.cached.cfgCtx.onunload()
    if inst.cached.controllers
      u.forEach inst.cached.controllers, (controller) ->
        if controller.unload
          controller.onunload preventDefault: noop

# shallow array compare, assumes strings
# A string-integer map is used to simplify the algorithm from
# two `O(n * log(n))` loops + an `O(n)` loop to just two O(n) loops
# with constant-time (or a super cheap `log(n)`) string key lookup.
arraySortCompare = (a, b) ->
  len = a.length
  if `len != b.length`
    return false
  i = 0
  cache = Object.create(null)
  while i < len
    cache[b[i]] = i++
  while `i != 0`
    if `cache[a[--i]] == undefined`
      return false
  true

builderElemIsDifferentEnough = (inst, dataAttrKeys) ->
  data = inst.data
  cached = inst.cached
  if `data.tag != cached.tag`
    return true
  if !arraySortCompare(dataAttrKeys, Object.keys(cached.attrs))
    return true
  if `data.attrs.id != cached.attrs.id`
    return true
  if `data.attrs.key != cached.attrs.key`
    return true
  if redrawStrategy is 'all'
    !cached.cfgCtx or `cached.cfgCtx.retain != true`
  else if redrawStrategy is 'diff'
    cached.cfgCtx and `cached.cfgCtx.retain == false`
  else
    false

objectBuildNewNode = (inst) ->
  node = objectCreateNode(inst)
  inst.builder.cached = objectReconstruct(inst, node, objectCreateAttrs(inst, node), objectBuildChildren(inst, node))
  node

objectBuild = (inst) ->
  builder = inst.builder
  isNew = `builder.cached.nodes.length == 0`
  node = if isNew then objectBuildNewNode(inst) else objectBuildUpdatedNode(inst)
  if isNew or builder.reattach and `node != null`
    insertNode builder.parent, node, builder.index
  builderScheduleConfigs builder, node, isNew
  builder.cached

objectCreateNode = (inst) ->
  data = inst.builder.data
  if `inst.ns == undefined`
    if data.attrs.is
      $document.createElement data.tag, data.attrs.is
    else
      $document.createElement data.tag
  else if data.attrs.is
    $document.createElementNS inst.ns, data.tag, data.attrs.is
  else
    $document.createElementNS inst.ns, data.tag

objectCreateAttrs = (inst, node) ->
  data = inst.builder.data
  if inst.hasKeys
    setAttributes node, data.tag, data.attrs, {}, inst.ns
  else
    data.attrs

objectMakeChild = (inst, node, shouldReattach) ->
  builder = inst.builder
  builderBuild buildContext(node, builder.data.tag, `undefined`, `undefined`, builder.data.children, builder.cached.children, shouldReattach, 0, (if builder.data.attrs.contenteditable then node else builder.editable), inst.ns, builder.cfgs)

objectBuildChildren = (inst, node) ->
  children = inst.builder.data.children
  if children?.length
    objectMakeChild inst, node, true
  else
    children

objectReconstruct = (inst, node, attrs, children) ->
  data = inst.builder.data
  cached =
    tag: data.tag
    attrs: attrs
    children: children
    nodes: [ node ]
  objectUnloadCachedControllers inst, cached
  if cached.children and !cached.children.nodes
    cached.children.nodes = []
  # edge case: setting value on <select> doesn't work before children
  # exist, so set it again after children have been created
  if data.tag is 'select' and data.attrs.value
    setAttributes node, data.tag, { value: data.attrs.value }, {}, inst.ns
  cached

unloadSingleCachedController = (controller) ->
  if controller.onunload and controller.onunload.$old
    controller.onunload = controller.onunload.$old

objectUnloadCachedControllers = (inst, cached) ->
  if inst.controllers.length
    cached.views = inst.views
    cached.controllers = inst.controllers
    u.forEach inst.controllers, unloadSingleCachedController

objectBuildUpdatedNode = (inst) ->
  cached = inst.builder.cached
  node = cached.nodes[0]
  if inst.hasKeys
    setAttributes node, inst.builder.data.tag, inst.builder.data.attrs, cached.attrs, inst.ns
  cached.children = objectMakeChild(inst, node, false)
  cached.nodes.intact = true
  if inst.controllers.length
    cached.views = inst.views
    cached.controllers = inst.controllers
  node

builderScheduleConfigs = (inst, node, isNew) ->
  data = inst.data
  cached = inst.cached
  # They are called after the tree is fully built
  config = data.attrs.config
  if u.isFunction(config)
    context = cached.cfgCtx = cached.cfgCtx or {}
    inst.cfgs.push ->
      config.call data, node, !isNew, context, cached

builderHandleTextNode = (inst) ->
  if `inst.cached.nodes.length == 0`
    builderHandleNonexistentNodes inst
  else if `inst.cached.valueOf() != inst.data.valueOf()` or inst.reattach
    builderReattachNodes inst
  else
    inst.cached.nodes.intact = true
    inst.cached

nodeHasBody = (node) ->
  !/^(AREA|BASE|BR|COL|COMMAND|EMBED|HR|IMG|INPUT|KEYGEN|LINK|META|PARAM|SOURCE|TRACK|WBR)$/.test(node)

builderHandleNonexistentNodes = (inst) ->
  nodes = undefined
  if inst.data.$trusted
    nodes = injectHTML(inst.parent, inst.index, inst.data)
  else
    nodes = [ $document.createTextNode(inst.data) ]
    if nodeHasBody(inst.parent.nodeName)
      insertNode inst.parent, nodes[0], inst.index
  cached = undefined
  if `typeof inst.data == 'string'` or `typeof inst.data == 'number'` or `typeof inst.data == 'boolean'`
    cached = new (inst.data.constructor)(inst.data)
  else
    cached = inst.data
  cached.nodes = nodes
  cached

builderReattachNodes = (inst) ->
  nodes = inst.cached.nodes
  if !inst.editable or `inst.editable != $document.activeElement`
    if inst.data.$trusted
      clear nodes, inst.cached
      nodes = injectHTML(inst.parent, inst.index, inst.data)
    else if `inst.pTag == 'textarea'`
      # <textarea> uses `value` instead of `nodeValue`.
      inst.parent.value = inst.data
    else if inst.editable
      # contenteditable nodes use `innerHTML` instead of `nodeValue`.
      inst.editable.innerHTML = inst.data
    else
      # was a trusted string
      if `nodes[0].nodeType == 1` or nodes.length > 1 or nodes[0].nodeValue.trim and !nodes[0].nodeValue.trim()
        clear inst.cached.nodes, inst.cached
        nodes = [ $document.createTextNode(inst.data) ]
      builderInjectTextNode inst, nodes[0]
  inst.cached = new (inst.data.constructor)(inst.data)
  inst.cached.nodes = nodes
  inst.cached

# This function was causing deopts in Chrome.

builderInjectTextNode = (inst, first) ->
  try
    insertNode inst.parent, first, inst.index
    first.nodeValue = inst.data
  catch e
    # IE erroneously throws error when appending an empty text node
    # after a null

getSubArrayCount = (item) ->
  if item.$trusted
    # fix offset of next element if item was a trusted string w/ more
    # than one HTML element. the first clause in the regexp matches
    # elements the second clause (after the pipe) matches text nodes
    match = item.match(/<[^\/]|\>\s*[^<]/g)
    if `match != null`
      return match.length
  else if u.isArray(item)
    return item.length
  else
    return 1

sortChanges = (a, b) ->
  a.action - (b.action) or a.index - (b.index)

shouldSetAttrDirectly = (attr) ->
  !/^(list|style|form|type|width|height)$/.test(attr)

autoredraw = (callback, object) ->
  (e) -> async ->
    try
      yield async callback.bind(object, e or event)
    finally
      redrawStrategy = 'diff'
      m.redraw()

trySetAttribute = (attr, dataAttr, cachedAttr, node, namespace, tag) ->
  if `attr == 'config'` or `attr == 'key'`
    # `config` and `key` aren't real attributes
    return
  else if u.isFunction(dataAttr) and `attr.slice(0, 2) == 'on'`
    # hook event handlers to the auto-redrawing system
    node[attr] = autoredraw(dataAttr, node)
  else if `attr == 'style'` and `dataAttr != null` and u.isObject(dataAttr)
    # handle `style: {...}`
    u.forOwn dataAttr, (value, rule) ->
      if `cachedAttr == null` or `cachedAttr[rule] != value`
        node.style[rule] = value
    for rule of cachedAttr
      if u.hasOwn.call(cachedAttr, rule)
        if !u.hasOwn.call(dataAttr, rule)
          node.style[rule] = ''
  else if `namespace != null`
    # handle SVG
    if `attr == 'href'`
      node.setAttributeNS 'http://www.w3.org/1999/xlink', 'href', dataAttr
    else
      node.setAttribute((if `attr == 'className'` then 'class' else attr), dataAttr)
  else if attr of node and shouldSetAttrDirectly(attr)
    # handle cases that are properties (but ignore cases where we
    # should use setAttribute instead):
    #
    # - list and form are typically used as strings, but are DOM
    #   element references in js
    # - when using CSS selectors (e.g. `m("[style='']")`), style is
    #   used as a string, but it's an object in js
    #
    # #348
    # don't set the value if not needed, otherwise cursor placement
    # breaks in Chrome
    if `tag != 'input'` or `node[attr] != dataAttr`
      node[attr] = dataAttr
  else
    node.setAttribute attr, dataAttr

trySetSingle = (attr, data, cached, node, namespace, tag) ->
  try
    trySetAttribute attr, data, cached, node, namespace, tag
  catch e
    # swallow IE's invalid argument errors to mimic HTML's
    # fallback-to-doing-nothing-on-invalid-attributes behavior
    if /\bInvalid argument\b/.test(e.message)
      throw e

setAttributes = (node, tag, dataAttrs, cachedAttrs, namespace) ->
  u.forOwn dataAttrs, (dataAttr, attr) ->
    cachedAttr = cachedAttrs[attr]
    if !(attr of cachedAttrs) or `cachedAttr != dataAttr`
      cachedAttrs[attr] = dataAttr
      trySetSingle attr, dataAttr, cachedAttr, node, namespace, tag
    else if `attr == 'value'` and `tag == 'input'` and `node.value != dataAttr`
      node.value = dataAttr
  cachedAttrs

clearSingle = (node) ->
  try
    node.parentNode.removeChild node
  catch e
    # ignore if this fails due to order of events (see
    # http://stackoverflow.com/questions/21926083/failed-to-execute-removechild-on-node)

clear = (nodes, cached) ->
  # If it's empty, there's nothing to clear
  if !nodes.length
    return
  cached = [].concat(cached)
  i = nodes.length - 1
  while i >= 0
    node = nodes[i]
    if `node != null` and node.parentNode
      clearSingle node
      if cached[i]
        unload cached[i]
    i--
  # release memory if nodes is an array. This check should fail if nodes
  # is a NodeList (see loop above)
  if nodes.length
    nodes.length = 0

unload = (cached) ->
  if cached.cfgCtx and u.isFunction(cached.cfgCtx.onunload)
    cached.cfgCtx.onunload()
    cached.cfgCtx.onunload = null
  if cached.controllers
    u.forEach cached.controllers, (controller) ->
      if u.isFunction(controller.onunload)
        controller.onunload preventDefault: noop
  if cached.children
    if u.isArray(cached.children)
      u.forEach cached.children, unload
    else if cached.children.tag
      unload cached.children

injectHTML = (parent, index, data) ->
  nextSibling = parent.childNodes[index]
  if nextSibling
    if `nextSibling.nodeType != 1`
      placeholder = $document.createElement('span')
      parent.insertBefore placeholder, nextSibling or null
      placeholder.insertAdjacentHTML 'beforebegin', data
      parent.removeChild placeholder
    else
      nextSibling.insertAdjacentHTML 'beforebegin', data
  else
    insertAdjacentBeforeEnd parent, data
  nodes = []
  while `parent.childNodes[index] != nextSibling`
    nodes.push parent.childNodes[index++]
  nodes


insertAdjacentBeforeEnd = do ->
  try
    $document.createRange().createContextualFragment 'x'
    return (parent, data) ->
      parent.appendChild $document.createRange().createContextualFragment(data)

  catch e
    return (parent, data) ->
      parent.insertAdjacentHTML 'beforeend', data


documentNode =
  appendChild: (node) ->
    if $document.documentElement and `$document.documentElement != node`
      $document.replaceChild node, $document.documentElement
    else
      $document.appendChild node
    @childNodes = $document.childNodes
  insertBefore: (node) ->
    @appendChild node
  childNodes: []

reset = (root) ->
  cacheKey = getCellCacheKey(root)
  clear root.childNodes, cellCache[cacheKey]
  cellCache[cacheKey] = undefined


m.render = (root, cell, forceRecreation) ->
  if !root
    throw new Error('Ensure the DOM element being passed to ' + 'm.route/m.mount/m.render exists.')
  configs = []
  id = getCellCacheKey(root)
  isDocumentRoot = `root == $document`
  node = undefined
  if isDocumentRoot or `root == $document.documentElement`
    node = documentNode
  else
    node = root
  if isDocumentRoot and `cell.tag != 'html'`
    cell =
      tag: 'html'
      attrs: {}
      children: cell
  if `cellCache[id] == undefined`
    clear node.childNodes
  if `forceRecreation == true`
    reset root
  cellCache[id] = builderBuild(buildContext(node, null, `undefined`, `undefined`, cell, cellCache[id], false, 0, null, `undefined`, configs))
  u.forEach configs, (config) ->
    config()

getCellCacheKey = (element) ->
  index = nodeCache.indexOf(element)
  if index < 0 then nodeCache.push(element) - 1 else index


initComponent = (component, root, index, isPrevented) ->
  isNullComponent = `component == null`
  if !isPrevented
    redrawStrategy = 'all'
    roots[index] = root
    component = `topComponent = component || { controller: noop }`

    controller = component.controller or noop

    vm = {}
    yield async(controller.bind(vm))
    controller = vm
    if `component == topComponent`
      controllers[index] = vm
      components[index] = component
    m.redraw()
    if isNullComponent
      removeRootElement root, index
    return controllers[index]
  if isNullComponent
    removeRootElement root, index
  return

m.trust = (value) ->
  value = new String(value)
  value.$trusted = true
  value

roots = []
components = []
controllers = []
unloaders = []
nodeCache = []
cellCache = {}

FRAME_BUDGET = 16
# 60 frames per second = 1 call per 16 ms
topComponent = undefined

mmount = (root, component) -> async =>
  if !root
    throw new Error('Please ensure the DOM element exists before ' + 'rendering a template into it.')
  index = roots.indexOf(root)
  if index < 0
    index = roots.length
  isPrevented = false
  ev = preventDefault: ->
    isPrevented = true
  u.forEach unloaders, (unloader) ->
    if unloader.ctrl?
      unloaderHandler unloader, ev
      unloader.ctrl.onunload = null
    return
  if isPrevented
    u.forEach unloaders, (unloader) ->
      unloader.ctrl.onunload = (ev) ->
        unloaderHandler unloader, ev


  else
    unloaders = []
  if controllers[index] and u.isFunction(controllers[index].onunload)
    controllers[index].onunload ev
  self = this
  yield initComponent(component, root, index, isPrevented)

removeRootElement = (root, index) ->
  roots.splice index, 1
  controllers.splice index, 1
  components.splice index, 1
  reset root
  nodeCache.splice getCellCacheKey(root), 1
  return

actuallyPerformRedraw = ->
  if `lastRedrawId != 0`
    $cancelAnimationFrame lastRedrawId
  `lastRedrawId = $requestAnimationFrame(redraw, FRAME_BUDGET)`
  return

resetLastRedrawId = ->
  `lastRedrawId = 0`
  return

attemptRedraw = (force) ->
  if lastRedrawId and !force
    performRedraw()
  else
    redraw()
    `lastRedrawId = $requestAnimationFrame(resetLastRedrawId, FRAME_BUDGET)`
  return

redraw = ->
  i = 0
  while i < roots.length
    root = roots[i]
    component = components[i]
    controller = controllers[i]
    if `controller != null`
      m.render root, if component.view then component.view(controller, [ controller ]) else ''
    i++
  # after rendering within a routed context, we need to scroll back to
  # the top, and fetch the document title for history.pushState

  `lastRedrawId = null`
  `lastRedrawCallTime = new Date()`
  redrawStrategy = 'diff'
  return

m.mount = m.module = mmount
# lastRedrawId is a positive number if a second redraw is requested before
# the next animation frame, or 0 if it's the first redraw and not an event
# handler
lastRedrawId = 0
lastRedrawCallTime = 0
# when setTimeout:
# only reschedule redraw if time between now and previous redraw is bigger
# than a frame, otherwise keep currently scheduled timeout
#
# when rAF:
# always reschedule redraw
performRedraw = if `$requestAnimationFrame == window.requestAnimationFrame` then actuallyPerformRedraw else (->
  if +new Date - lastRedrawCallTime > FRAME_BUDGET
    actuallyPerformRedraw()
  return
)


# ---
# generated by js2coffee 2.2.0