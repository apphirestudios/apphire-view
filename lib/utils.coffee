type = {}.toString
hasOwn = {}.hasOwnProperty
module.exports =
  type: type.bind @
  forKeys: (list, f) ->
    i = 0
    while i < list.length
      attrs = list[i]
      attrs = attrs and attrs.attrs
      if attrs and `attrs.key != null` and f(attrs, i)
        break
      i++

  forEach: (list, f) ->
    i = 0
    while i < list.length
      f list[i], i
      i++

  forOwn: (obj, f) ->
    for prop of obj
      if hasOwn.call(obj, prop)
        f obj[prop], prop


  isFunction: (object) ->
    typeof object is 'function'

  isObject: (object) ->
    type.call(object) is '[object Object]'

  isString: (object) ->
    type.call(object) == '[object String]'

  isArray: Array.isArray or (object) ->
    type.call(object) is '[object Array]'