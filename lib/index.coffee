mithril = require './mithril'
extend = (object, properties) ->
  for key, val of properties
    object[key] = val
  object


hasOwn = {}.hasOwnProperty
forOwn = (obj, f) ->
  for prop of obj
    if hasOwn.call(obj, prop)
      f obj[prop], prop
  return

merge = (options, overrides) ->
  extend (extend {}, options), overrides

constructMithrilElement = (vdd) ->
  cell =
    tag: 'div'
    attrs: {}
    children: vdd.children
  classAttr =    if 'class' of vdd.attrs then 'class' else 'className'
  assignAttrs cell.attrs, vdd.attrs, classAttr, parseSelector(vdd.tag, cell)
  delete cell.attrs.vdd
  return cell

parseSelector = (tag, cell) ->
  classes = []
  parser = /(?:(^|#|\.)([^#\.\[\]]+))|(\[.+?\])/g
  match = undefined
  while (match = parser.exec(tag)) != null
    if match[1] == '' and match[2] != null
      cell.tag = match[2]
    else if match[1] == '#'
      cell.attrs.id = match[2]
    else if match[1] == '.'
      classes.push match[2]
    else if match[3][0] == '['
      pair = /\[(.+?)(?:=("|'|)(.*?)\2)?\]/.exec(match[3])
      cell.attrs[pair[1]] = pair[3] or (if pair[2] then '' else true)
  return classes

assignAttrs = (target, attrs, classAttr, classes) ->
  hasClass = false
  if hasOwn.call(attrs, classAttr)
    value = attrs[classAttr]
    if value != null and value != ''
      hasClass = true
      classes.push value
  forOwn attrs, (value, attr) ->
    target[attr] = if attr == classAttr and hasClass then '' else value
    return
  if classes.length
    target[classAttr] = classes.join(' ')
  return


handleFunctionResults = (res, element)->
  return if not res?
  if res.tag or res.subtree?
    return element.children.push res
  if typeof res is 'string' or typeof res is 'number'
    return element.children.push(res)
  if res.constructor is Object
    if res.view? or res.controller?
      return element.children.push(res)
    for k, v of res
      element.attrs[k] = v

buildNode = (sub, element, vdd)->
  switch sub.type
    when 'jade'
      element.children.push buildElement(sub, vdd)
    when 'hash'
      for k, v of sub.hash()
        element.attrs[k] = v
    when 'func'
      if (res = sub.func())?
        if res instanceof Array
          for subRes in res
            handleFunctionResults(subRes, element)
        else
          handleFunctionResults(res, element)

    when 'plain'
      element.children.push(sub.plain)
    when 'blank'
      noop = true
    when 'comment'
      noop = true
  return
buildElement = (node, vdd)->
  element =
    tag: node.tag
    attrs:
      vdd: vdd
    children: []
  if node.sub?.length > 0
    for sub, index in node.sub
      buildNode(sub, element, vdd.sub[index])

  decoratedElement = decorate(element)
  return constructMithrilElement(decoratedElement)

decorate = (element) ->
    attrs = element.attrs or {}
    for name, transformer of renderer.transformer
      element = transformer(element)
      if not element.tag
        throw new Error 'Transformer ' + name + 'must return objects containing tag selector'

    for attr, value of attrs
      if renderer.attribute[attr] then renderer.attribute[attr](element.tag, element.attrs, element.children)


    for attrName, fn of renderer.decorator
      originalAttr = attrs[attrName]

      attrs[attrName] = fn.bind originalAttr: originalAttr

    if element.tag.indexOf('uma') == 0
      if renderer.component[element.tag]
        componentRootElement =
          tag : element.tag
          attrs: {}
          children: [mithril.component(renderer.component[element.tag], element.attrs, element.children)]

        return componentRootElement

      else
        throw new Error 'Component name ' + element.tag + ' is not registered'
    else
      return element

module.exports = renderer =
  component: {}
  attribute: {}
  decorator: {}
  transformer: {}

  constructMithrilElement: constructMithrilElement
  serializeObject: require './object-to-text'

  construct: (node, vdd, modulePath) ->
    #cloned = $.extend(true, {}, vdd);
    #log cloned
    vdom = buildElement(node, vdd or node)

  compute: (fn)->
    if fn then fn()
    mithril.redraw()

  redraw: mithril.redraw.bind @
  mount: mithril.mount.bind @


