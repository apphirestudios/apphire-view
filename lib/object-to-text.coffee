objectToText = (obj) ->
  string = []
  if typeof obj == 'object' and obj.join == undefined
    string.push '{'
    for prop of obj
      `prop = prop`
      string.push prop, ': ', objectToText(obj[prop]), ','
    string.push '}'
    #is array
  else if typeof obj == 'object' and !(obj.join == undefined)
    string.push '['
    for prop of obj
      `prop = prop`
      string.push objectToText(obj[prop]), ','
    string.push ']'
    #is function
  else if typeof obj == 'function'
    string.push obj.toString()
    #all other values can be done with JSON.stringify
  else
    string.push JSON.stringify(obj)
  string.join ''


module.exports = objectToText